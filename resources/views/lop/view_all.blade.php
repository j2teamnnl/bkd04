@extends('layout.master')

@section('content')
<h1>
	đây là danh sách lớp
</h1>
<a href="{{ route('lop.view_insert') }}">
	Thêm
</a>
<table class="table">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Tên tiếng Anh</th>
		<th>Sửa</th>
	</tr>
	@foreach ($array_lop as $lop)
		<tr>
			<td>
				{{ $lop->ma }}
			</td>
			<td>
				{{ $lop->ten }}
			</td>
			<td>
				{{ $lop->ten_tieng_anh }}
			</td>
			<td>
				<a href="{{ route('lop.view_update',['ma' => $lop->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('lop.delete',['ma' => $lop->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>

@endsection