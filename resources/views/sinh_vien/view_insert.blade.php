<form action="{{ route('sinh_vien.process_insert') }}" method="post">
	{{ csrf_field() }}
	Tên
	<input type="text" name="ten">
	<br>
	Email
	<input type="email" name="email">
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh">
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1">Nam
	<input type="radio" name="gioi_tinh" value="0">Nữ
	<br>
	Lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}">
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<button>Thêm</button>
</form>