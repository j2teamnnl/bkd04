<form action="{{ route('sinh_vien.process_update',['ma' => $sinh_vien->ma]) }}" method="post">
	{{ csrf_field() }}
	Tên
	<input type="text" name="ten" value="{{ $sinh_vien->ten }}">
	<br>
	Email
	<input type="email" name="email" value="{{ $sinh_vien->email }}">
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh" value="{{ $sinh_vien->ngay_sinh }}">
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1"
	@if ($sinh_vien->gioi_tinh==1)
		checked 
	@endif
	>Nam
	<input type="radio" name="gioi_tinh" value="0"
	@if ($sinh_vien->gioi_tinh==0)
		checked 
	@endif
	>Nữ
	<br>
	Lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}"
			@if ($sinh_vien->ma_lop==$lop->ma)
				selected 
			@endif
			>
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<button>Sửa</button>
</form>