@extends('layout.master')

@section('content')
<h1>
	đây là danh sách sinh viên
</h1>
<a href="{{ route('sinh_vien.view_insert') }}">
	Thêm
</a>
<table class="table">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Email</th>
		<th>Tuổi</th>
		<th>Giới tính</th>
		<th>Tên lớp</th>
		<th>Sửa</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien->ma }}
			</td>
			<td>
				{{ $sinh_vien->ten }}
			</td>
			<td>
				{{ $sinh_vien->email }}
			</td>
			<td>
				{{ $sinh_vien->tuoi }}
			</td>
			<td>
				{{ $sinh_vien->ten_gioi_tinh }}
			</td>
			<td>
				{{ $sinh_vien->lop->ten }}
			</td>
			<td>
				<a href="{{ route('sinh_vien.view_update',['ma' => $sinh_vien->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('sinh_vien.delete',['ma' => $sinh_vien->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>

@endsection