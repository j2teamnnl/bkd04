<form action="{{ route('mon.process_update_mon_nganh') }}" method="post">
	{{ csrf_field() }}
	Chọn môn
	<select name="ma_mon">
		@foreach ($array_mon as $mon)
			<option value="{{ $mon->ma }}">
				{{ $mon->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn ngành
	<select name="ma_nganh">
		@foreach ($array_nganh as $nganh)
			<option value="{{ $nganh->ma }}">
				{{ $nganh->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Học kỳ
	<input type="number" name="hoc_ky">
	<br>
	<button>Cập nhật</button>
</form>