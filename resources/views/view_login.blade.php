
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Urora - Responsive Bootstrap 4 Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />


    </head>
    <body>


    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">
        <div class="display-table">
            <div class="display-table-cell">
                <diV class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="assets/images/extra.png" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center pt-3">
                                        <a href="index.html">
                                            <img src="assets/images/logo-dark.png" alt="logo" height="22" />
                                        </a>
                                    </div>
                                    @if(Session::has('error'))
                                        <h1>
                                            {{ Session::get('error') }}
                                        </h1>
                                    @endif
                                    <div class="px-3 pb-3">
                                        <form class="form-horizontal m-t-20 mb-0" action="{{ route('process_login') }}" method="post">
                                            {{ csrf_field() }}
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <input class="form-control" type="email" name="email" required="" placeholder="Username">
                                                </div>
                                            </div>
                    
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <input class="form-control" type="password" name="mat_khau" required="" placeholder="Password">
                                                </div>
                                            </div>
                    
                    
                                            <div class="form-group text-right row m-t-20">
                                                <div class="col-12">
                                                    <button class="btn btn-primary btn-raised btn-block waves-effect waves-light" type="submit">Log In</button>
                                                </div>
                                            </div>
                    
                                        </form>
                                    </div>
                    
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </diV>
            </div>
        </div>
    </div>
        
    </body>
</html>