@if (Session::has('error'))
	<h1>
		{{ Session::get('error') }}
	</h1>
@endif
<form action="{{ route('giao_vien.view_diem_danh') }}" method="post">
	{{ csrf_field() }}
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}">
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn môn
	<select name="ma_mon">
		@foreach ($array_mon as $mon)
			<option value="{{ $mon->ma }}">
				{{ $mon->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Chọn</button>
</form>