@if (Session::has('error'))
	<h1>
		{{ Session::get('error') }}
	</h1>
@endif
<form action="{{ route('giao_vien.view_diem_danh') }}" method="post">
	{{ csrf_field() }}
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}" 
			@if ($lop->ma == $ma_lop)
				selected 
			@endif>
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn môn
	<select name="ma_mon">
		@foreach ($array_mon as $mon)
			<option value="{{ $mon->ma }}" 
			@if ($mon->ma == $ma_mon)
				selected 
			@endif>
				{{ $mon->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Chọn</button>
</form>
<form action="{{ route('giao_vien.process_diem_danh') }}" method="post">
	{{ csrf_field() }}
	<input type="hidden" name="ma_lop" value="{{ $ma_lop }}">
	<input type="hidden" name="ma_mon" value="{{ $ma_mon }}">
<table>
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Tình trạng đi học</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien->ma }}
			</td>
			<td>
				{{ $sinh_vien->ten }}
			</td>
			<td>
				<input type="radio" name="tinh_trang_di_hoc[{{ $sinh_vien->ma }}]" value="1" checked>Đi học
				<input type="radio" name="tinh_trang_di_hoc[{{ $sinh_vien->ma }}]" value="2"
				@if (isset($array[$sinh_vien->ma]) && $array[$sinh_vien->ma]==2)
					checked 
				@endif>Nghỉ
			</td>
		</tr>
	@endforeach
</table>
<button>Điểm danh</button>
<script type="text/javascript">
	
	
</script>
</form>