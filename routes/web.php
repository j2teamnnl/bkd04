<?php

Route::get('view_sinh_vien_import','SinhVienController@view_sinh_vien_import');
Route::post('process_sinh_vien_import','SinhVienController@process_sinh_vien_import')->name('process_sinh_vien_import');

Route::group(['middleware' => 'CheckLogin'],function(){

	Route::group(['prefix' => 'lop','as' => 'lop.'],function(){
		Route::get('','LopController@view_all')->name('view_all');
		Route::get('view_insert','LopController@view_insert')->name('view_insert');
		Route::post('process_insert','LopController@process_insert')->name('process_insert');
		Route::get('view_update/{ma}','LopController@view_update')->name('view_update');
		Route::post('process_update/{ma}','LopController@process_update')->name('process_update');
		Route::get('delete/{ma}','LopController@delete')->name('delete');
	});

	Route::group(['prefix' => 'sinh_vien','as' => 'sinh_vien.'],function(){
		Route::get('','SinhVienController@view_all')->name('view_all');
		Route::get('view_insert','SinhVienController@view_insert')->name('view_insert');
		Route::post('process_insert','SinhVienController@process_insert')->name('process_insert');
		Route::get('view_update/{ma}','SinhVienController@view_update')->name('view_update');
		Route::post('process_update/{ma}','SinhVienController@process_update')->name('process_update');
		Route::get('delete/{ma}','SinhVienController@delete')->name('delete');
	});

	Route::group(['prefix' => 'mon','as' => 'mon.'],function(){
		Route::get('','MonController@view_all')->name('view_all');
		Route::get('view_update_mon_nganh','MonController@view_update_mon_nganh')->name('view_update_mon_nganh');
		Route::post('process_update_mon_nganh','MonController@process_update_mon_nganh')->name('process_update_mon_nganh');
	});
	Route::get('view_upload_avatar','Controller@view_upload_avatar')->name('view_upload_avatar');
	Route::post('process_upload_avatar','Controller@process_upload_avatar')->name('process_upload_avatar');
	Route::group(['prefix' => 'phan_cong','as' => 'phan_cong.'],function(){
		Route::get('','PhanCongController@view_phan_cong')->name('view_phan_cong');
		Route::post('','PhanCongController@process_phan_cong')->name('process_phan_cong');
	});
});


Route::get('','Controller@master');
Route::get('view_login','Controller@view_login')->name('view_login');
Route::post('process_login','Controller@process_login')->name('process_login');

Route::group(['middleware' => 'CheckGiaoVien', 'prefix' => 'giao_vien','as' => 'giao_vien.'],function(){
	Route::get('choose_diem_danh','DiemDanhController@choose_diem_danh')->name('choose_diem_danh');
	Route::post('view_diem_danh','DiemDanhController@view_diem_danh')->name('view_diem_danh');
	Route::post('process_diem_danh','DiemDanhController@process_diem_danh')->name('process_diem_danh');
});