<?php

namespace App\Imports;

use App\Models\SinhVien;
use App\Models\Lop;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SinhVienImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $row = array_filter($row);
        if(!empty($row)){
            $array = [
                'ten' => $row['sinh_vien'],
                'ngay_sinh' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['ngay_sinh'])->format('Y-m-d'),
                'gioi_tinh' => ($row['gioi_tinh']=='Nam') ? 1 : 0,
                // lấy lớp đầu tiên có tên bằng cột lớp ở trong Excel
                // rồi lấy mã của lớp đấy
                // value: tìm thằng đầu tiên rồi lấy cột mã
                'ma_lop' => Lop::firstOrCreate(['ten' => $row['lop']])->ma,
            ];
            return new SinhVien($array);
        }
    }
}
