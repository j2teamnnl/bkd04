<?php

namespace App\Http\Controllers;

use App\Models\GiaoVien;
use App\Models\Mon;
use App\Models\Lop;
use App\Models\PhanCong;
use Illuminate\Http\Request;

class PhanCongController extends Controller
{
    public function view_phan_cong()
    {
        $array_mon       = Mon::get();
        $array_giao_vien = GiaoVien::get();
        $array_lop       = Lop::get();

        return view('phan_cong.view_phan_cong',compact('array_mon','array_giao_vien','array_lop'));
    }
    public function process_phan_cong(Request $rq)
    {
        PhanCong::updateOrCreate([
            'ma_lop' => $rq->ma_lop,
            'ma_mon' => $rq->ma_mon,
        ],[
            'ma_giao_vien' => $rq->ma_giao_vien
        ]);

    }
}
