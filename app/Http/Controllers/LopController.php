<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;


class LopController
{
	public function view_all()
	{
		$array_lop = Lop::get();
		return view('lop.view_all',compact('array_lop'));
	}
	public function view_insert()
	{
		return view('view_insert');
	}
	public function process_insert(Request $rq)
	{
		Lop::create($rq->all());

		return redirect()->route('view_all');
	}
	public function view_update($ma)
	{
		// $lop = Lop::where('ma','=',$ma)->first();

		$lop = Lop::find($ma);

		return view('view_update',compact('lop'));
	}
	public function process_update($ma, Request $rq)
	{
		Lop::find($ma)->update($rq->all());

		return redirect()->route('view_all');
	}
	public function delete($ma)
	{
		// Lop::find($ma)->delete();

		Lop::destroy($ma);

		return redirect()->route('view_all');
	}
}
