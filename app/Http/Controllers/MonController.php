<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mon;
use App\Models\Nganh;


class MonController 
{
	public function view_all()
	{
		$array_mon = Mon::get();
		return view('mon.view_all',compact('array_mon'));
	}
	public function view_update_mon_nganh()
	{
		$array_mon = Mon::get();
		$array_nganh = Nganh::get();
		return view('mon.view_update_mon_nganh',compact('array_mon','array_nganh'));
	}
	public function process_update_mon_nganh(Request $rq)
	{
		// attach: insert
		// detach: delete
		// sync: insert and delete / nếu có rồi thì không insert
		// syncWithoutDetaching: insert not delete / nếu có rồi thì không insert
		Mon::find($rq->ma_mon)
		->array_nganh()
		->syncWithoutDetaching([
			$rq->ma_nganh => [
				'hoc_ky' => $rq->hoc_ky
			]
		]);
	}
}
