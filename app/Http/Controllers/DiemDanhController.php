<?php

namespace App\Http\Controllers;

use App\Models\DiemDanh;
use App\Models\DiemDanhChiTiet;
use App\Models\PhanCong;
use App\Models\SinhVien;
use Illuminate\Http\Request;
use Session;
use Exception;

class DiemDanhController extends Controller
{
    public function choose_diem_danh()
    {
        $array_lop = PhanCong::where('ma_giao_vien',Session::get('ma_giao_vien'))
        ->join('lop','lop.ma','phan_cong.ma_lop')
        ->select('lop.ma','lop.ten')
        ->distinct()
        ->get();
        $array_mon = PhanCong::where('ma_giao_vien',Session::get('ma_giao_vien'))
        ->join('mon','mon.ma','phan_cong.ma_mon')
        ->select('mon.ma','mon.ten')
        ->distinct()
        ->get();

        return view('giao_vien.choose_diem_danh',compact('array_lop','array_mon'));
    }
    public function view_diem_danh(Request $rq)
    {
        $ma_lop = $rq->ma_lop;
        $ma_mon = $rq->ma_mon;
        try {
            PhanCong::where('ma_lop',$ma_lop)
            ->where('ma_mon',$ma_mon)
            ->where('ma_giao_vien',Session::get('ma_giao_vien'))
            ->firstOrFail();
        } catch (Exception $e) {
            return redirect()->route('giao_vien.choose_diem_danh')->with('error','Dont have permission');
        }
        $array_lop = PhanCong::where('ma_giao_vien',Session::get('ma_giao_vien'))
        ->join('lop','lop.ma','phan_cong.ma_lop')
        ->select('lop.ma','lop.ten')
        ->distinct()
        ->get();
        $array_mon = PhanCong::where('ma_giao_vien',Session::get('ma_giao_vien'))
        ->join('mon','mon.ma','phan_cong.ma_mon')
        ->select('mon.ma','mon.ten')
        ->distinct()
        ->get();

        $array_sinh_vien = SinhVien::where('sinh_vien.ma_lop',$rq->ma_lop)
        ->get();

        $array_diem_danh = DiemDanh::where('ma_lop',$rq->ma_lop)
        ->join('diem_danh_chi_tiet','diem_danh_chi_tiet.ma_diem_danh','diem_danh.ma')
        ->where('ma_mon',$rq->ma_mon)
        ->where('ngay',date('Y-m-d'))
        ->select('diem_danh_chi_tiet.ma_sinh_vien','tinh_trang_di_hoc')
        ->get();

        $array = [];
        foreach ($array_diem_danh as $diem_danh) {
            $array[$diem_danh->ma_sinh_vien] = $diem_danh->tinh_trang_di_hoc;
        }

        return view('giao_vien.view_diem_danh',compact(
            'ma_lop',
            'ma_mon',
            'array_lop',
            'array_mon',
            'array_sinh_vien',
            'array',
        ));
    }
    public function process_diem_danh(Request $rq)
    {
        $ma_diem_danh = DiemDanh::firstOrCreate([
            'ma_lop' => $rq->ma_lop,
            'ma_mon' => $rq->ma_mon,
            'ma_giao_vien' => Session::get('ma_giao_vien'),
            'ngay' => date('Y-m-d'),
        ])->ma;


        foreach ($rq->tinh_trang_di_hoc as $ma_sinh_vien => $tinh_trang_di_hoc) {
            DiemDanhChiTiet::updateOrCreate([
                'ma_diem_danh' => $ma_diem_danh,
                'ma_sinh_vien' => $ma_sinh_vien,
            ],[
                'tinh_trang_di_hoc' => $tinh_trang_di_hoc,
            ]);
        }
    }
}
