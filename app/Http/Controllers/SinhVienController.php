<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\SinhVien;
use App\Imports\SinhVienImport;
use Maatwebsite\Excel\Facades\Excel;


class SinhVienController
{
	public function view_all()
	{
		$array_sinh_vien = SinhVien::with('lop')->get();
		return view('sinh_vien.view_all',compact('array_sinh_vien'));
	}
	public function view_insert()
	{
		$array_lop = Lop::get();
		return view('sinh_vien.view_insert',compact('array_lop'));
	}
	public function process_insert(Request $rq)
	{
		SinhVien::create($rq->all());

		return redirect()->route('sinh_vien.view_all');
	}
	public function view_update($ma)
	{
		$sinh_vien = SinhVien::find($ma);
		$array_lop = Lop::get();

		return view('sinh_vien.view_update',compact('sinh_vien','array_lop'));
	}
	public function process_update($ma, Request $rq)
	{
		SinhVien::find($ma)->update($rq->all());

		return redirect()->route('sinh_vien.view_all');
	}
	public function delete($ma)
	{
		// Lop::find($ma)->delete();

		Lop::destroy($ma);

		return redirect()->route('view_all');
	}
	public function view_sinh_vien_import()
	{
		return view('sinh_vien.view_sinh_vien_import');
	}
	public function process_sinh_vien_import(Request $rq)
	{
		Excel::import(new SinhVienImport, $rq->file_sinh_vien);
	}
}
