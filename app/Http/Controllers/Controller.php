<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\GiaoVien;
use Exception;
use Session;
use Storage;

class Controller
{
	public function master()
	{
		return view('layout.master');
	}
	public function view_login()
	{
		return view('view_login');
	}
	public function process_login(Request $rq)
	{
		$admin = Admin::where('email',$rq->email)
		->where('mat_khau',$rq->mat_khau)
		->first();
		if(!empty($admin)){
			dd("1");
			Session::put('ma',$admin->ma);
			Session::put('ten',$admin->ten);
			Session::put('anh',$admin->anh);
			Session::put('cap_do',$admin->cap_do);

			return redirect()->route('lop.view_all');
		}
		else{
			$giao_vien = GiaoVien::where('email',$rq->email)
			->where('mat_khau',$rq->mat_khau)
			->first();
			if(!empty($giao_vien)){
				Session::put('ma_giao_vien',$giao_vien->ma);
				Session::put('ten',$giao_vien->ten);

				return redirect()->route('giao_vien.choose_diem_danh');
			}
		}
		return redirect()->route('view_login')->with('error','Login fail');
		
	}
	public function view_upload_avatar()
	{
		return view('view_upload_avatar');
	}
	public function process_upload_avatar(Request $rq)
	{
		$duong_link = Storage::disk('public')->put('avatars', $rq->anh);
		Admin::where('ma',Session::get('ma'))
		->update([
			'anh' => $duong_link
		]);
		Session::put('anh', $duong_link);
	}
}
