<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mon extends Model
{
    protected $table = 'mon';
    protected $primaryKey = 'ma';
    public function array_nganh()
    {
        return $this->belongsToMany('App\Models\Nganh','mon_nganh','ma_mon','ma_nganh');
    }
}
