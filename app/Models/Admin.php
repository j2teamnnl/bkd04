<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';
    protected $fillable = [
    	'ten',
    	'email',
    	'mat_khau',
    	'anh',
    	'cap_do',
    ];	
    public $timestamps = false;
    protected $primaryKey = 'ma';

}
