<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiemDanh extends Model
{
    protected $table = 'diem_danh';
    protected $fillable = [
    	'ma_lop',
    	'ma_giao_vien',
    	'ma_mon',
    	'ngay',
    ];
    public $timestamps = false;

    protected $primaryKey = 'ma';
}
