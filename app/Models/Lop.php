<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
    protected $table = 'lop';
    protected $fillable = [
    	'ten',
    	'ten_tieng_anh',
    ];	
    public $timestamps = false;
    protected $primaryKey = 'ma';

}
