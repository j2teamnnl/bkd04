<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhanCong extends Model
{
	use Traits\HasCompositePrimaryKey;
    protected $table = 'phan_cong';
    public $timestamps = false;

    protected $fillable = ['ma_lop','ma_mon','ma_giao_vien'];

    protected $primaryKey = ['ma_lop','ma_mon'];

}
