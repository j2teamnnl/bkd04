<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
    protected $table = 'sinh_vien';
    protected $fillable = [
    	'ten',
    	'email',
    	'ngay_sinh',
    	'gioi_tinh',
    	'ma_lop',
    ];	
    public $timestamps = false;
    protected $primaryKey = 'ma';
    public function lop()
    {
        return $this->belongsTo('App\Models\Lop', 'ma_lop');
    }
    public function getTenGioiTinhAttribute()
    {
        if ($this->gioi_tinh==1) {
            return "Nam";
        } else {
            return "Nữ";
        }
    }
    public function getTuoiAttribute()
    {
        $age = date_diff(date_create($this->ngay_sinh), date_create('now'))->y;
        return $age;
    }
}
