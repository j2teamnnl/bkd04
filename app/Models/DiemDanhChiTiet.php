<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiemDanhChiTiet extends Model
{
	use Traits\HasCompositePrimaryKey;
    protected $table = 'diem_danh_chi_tiet';
    protected $fillable = [
    	'ma_diem_danh',
    	'ma_sinh_vien',
    	'tinh_trang_di_hoc',
    ];
    public $timestamps = false;

    protected $primaryKey = ['ma_diem_danh','ma_sinh_vien'];
}
