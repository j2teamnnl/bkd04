<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiemDanhTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diem_danh', function (Blueprint $table) {
            $table->increments('ma');
            $table->integer('ma_lop')->unsigned();
            $table->integer('ma_mon')->unsigned();
            $table->foreign(['ma_lop','ma_mon'])
            ->references(['ma_lop','ma_mon'])
            ->on('phan_cong');
            $table->integer('ma_giao_vien')->unsigned();
            $table->foreign('ma_giao_vien')
            ->references('ma')
            ->on('giao_vien');
            $table->date('ngay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diem_danhs');
    }
}
