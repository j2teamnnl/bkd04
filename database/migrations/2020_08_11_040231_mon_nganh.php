<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MonNganh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mon_nganh', function (Blueprint $table) {
            $table->integer('ma_mon')->unsigned();
            $table->integer('ma_nganh')->unsigned();
            $table->foreign('ma_mon')->references('ma')->on('mon');
            $table->foreign('ma_nganh')->references('ma')->on('nganh');
            $table->primary(['ma_mon','ma_nganh']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
