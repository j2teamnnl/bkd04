<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiemDanhChiTietTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diem_danh_chi_tiet', function (Blueprint $table) {
            $table->integer('ma_diem_danh')->unsigned();
            $table->foreign('ma_diem_danh')
            ->references('ma')
            ->on('diem_danh');
            $table->integer('ma_sinh_vien')->unsigned();
            $table->foreign('ma_sinh_vien')
            ->references('ma')
            ->on('sinh_vien');
            $table->integer('tinh_trang_di_hoc');
            $table->primary(['ma_diem_danh','ma_sinh_vien']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diem_danh_chi_tiets');
    }
}
